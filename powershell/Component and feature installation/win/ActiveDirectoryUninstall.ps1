param(
    [string]$local_admin_password='<insert_some_password_here_or_pass_it_as_parameter>'
)

$secure_password=ConvertTo-SecureString -String $local_admin_password -Force -AsPlainText
Import-Module ActiveDirectory
Uninstall-ADDSDomainController -LocalAdministratorPassword $secure_password -Force
#after reboot
Remove-WindowsFeature -Name AD-Domain-Services -IncludeManagementTools
Add-Computer -WorkgroupName "asdfdf" -Force
Stop-Computer -Force

