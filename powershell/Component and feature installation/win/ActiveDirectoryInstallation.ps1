param(
    [string]$domain_name='example.com',
    [string]$directory_restore_password='<insert_some_password_here_or_pass_it_as_parameter>'
)

Install-WindowsFeature -Name AD-Domain-Services -IncludeAllSubFeature -IncludeManagementTools
if ($?) {
    $secure_password=ConvertTo-SecureString -String $directory_restore_password -Force -AsPlainText
    Import-Module ActiveDirectory
    Install-ADDSDomainController -DomainName $domain_name -InstallDNS -SafeModeAdministratorPassword $secure_password -Confirm:$false
}
