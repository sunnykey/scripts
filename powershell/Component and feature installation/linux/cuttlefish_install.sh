#install Docker first

git clone https://github.com/mlandauer/cuttlefish
sudo apt update
sudo apt install -y docker-compose
sudo docker-compose run web bundle exec rake db:create db:schema:load
#Wait until DB will be created. If ignore this, app may not start
sudo docker-compose run web bundle exec rake db:seed
sudo docker-compose up &
