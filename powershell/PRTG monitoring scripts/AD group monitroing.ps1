$ad_group_log_path='c:\tmp\domain_admins_member.txt'
$last_ad_group_count=0
$last_value_path='c:\tmp\domain_admins_member_last_value.txt'


if (!(Test-Path $ad_group_log_path)) {
    Write-Output "-1:$ad_group_log_path not found"
    exit 1
}
$group_user_count=(Get-Content $ad_group_log_path | Measure-Object).Count
if (!(Test-Path $last_value_path)) {
    $last_ad_group_count=$group_user_count
} else {
    $last_ad_group_count=Get-Content $last_value_path
}
$group_user_count | Out-File -FilePath $last_value_path
if ($last_ad_group_count -eq $group_user_count) {
    Write-Output "${group_user_count}:OK (Group members count is the same as before)"
} else {
    Write-Output "${group_user_count}:Group members count was changed"
    exit 2
}