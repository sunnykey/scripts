$log_path='C:\inetpub\logs\SMTPSVC1'
$Hours_back_for_log_search=-1
$Log_search_pattern="limit"
$prtg_file='C:\inetpub\logs\PRTG\prtg_report.txt'

if (Get-ChildItem $log_path | Where-Object LastWriteTime -gt (get-date).AddHours($Hours_back_for_log_search) | Select-String $Log_search_pattern | Select-Object -First 1) {
    'AWS send limit reached' | Out-File -FilePath $prtg_file -Encoding utf8
} else {
    'Ok' | Out-File -FilePath $prtg_file -Encoding utf8
}

