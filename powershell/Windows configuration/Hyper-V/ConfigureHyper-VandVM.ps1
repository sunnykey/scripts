#Install-WindowsFeature Hyper-V -IncludeAllSubfeatures -IncludeManagementTools -Restart

#after restart
$external_switch_name='vSwitch-external'
$intenal_switch_name='vSwitch-internal'
$hyperv_folder_path="C:\Hyper-V"
$debian_iso_path="c:\iso\debian11.iso"
$pfsense_iso_path="c:\iso\pfsense.iso"
$win2022_iso_path="c:\iso\win2022.iso"
$vm_gw_name='fms-gw'
$hyperv_vm_settings_json='
{
	"VM": [{
		"name": "fms-gw",
		"type": "linux",
		"cpu": 4,
		"memory": 4,
		"hdd": 64,
		"iso": "pfsense"
	}, {
		"name": "fms-ad1",
		"type": "windows",
		"cpu": 2,
		"memory": 4,
		"hdd": 32,
		"iso": "win2022"
	}, {
		"name": "fms-rdgw",
		"type": "windows",
		"cpu": 2,
		"memory": 4,
		"hdd": 32,
		"iso": "win2022"
	}, {
		"name": "fms-mgmt",
		"type": "windows",
		"cpu": 4,
		"memory": 8,
		"hdd": 128,
		"iso": "win2022"
	}, {
		"name": "fms-trueconf",
		"type": "linux",
		"cpu": 16,
		"memory": 16,
		"hdd": 64,
		"iso": "debian11"
	}]
}'

$hyperv_vhd_folder_path=$hyperv_folder_path+'\HDD'
$hyperv_vm=$hyperv_vm_settings_json | ConvertFrom-Json

Import-Module Hyper-V
# $eth=(Get-NetIPAddress -AddressFamily IPv4 | Where-Object PrefixOrigin -ne WellKnown).InterfaceAlias
# New-VMSwitch -Name $external_switch_name -NetAdapterName $eth
# New-VMSwitch -Name $intenal_switch_name -SwitchType Internal

New-Item -Path $hyperv_vhd_folder_path -ItemType Directory -ErrorAction SilentlyContinue
Get-VMHost | Set-VMHost -VirtualMachinePath $hyperv_folder_path -VirtualHardDiskPath $hyperv_vhd_folder_path
foreach ($vm in $hyperv_vm.VM) {
    $vhdx_path=$hyperv_vhd_folder_path+"\"+$vm.name+'.vhdx'
    $vhdx_size=1gb*$vm.hdd
    $vm_memory=1gb*$vm.memory
    $vhd=New-VHD -Path $vhdx_path -SizeBytes $vhdx_size
    $new_hyperv_vm=New-VM -Name $vm.name `
        -SwitchName $intenal_switch_name `
        -Generation 2 `
        -VHDPath $vhd.Path `
        -MemoryStartupBytes $vm_memory
    switch ($vm.iso) {
        'debian11' { Add-VMDvdDrive -VMName $new_hyperv_vm.Name -Path $debian_iso_path }
        'pfsense' { Add-VMDvdDrive -VMName $new_hyperv_vm.Name -Path $pfsense_iso_path }
        default { Add-VMDvdDrive -VMName $new_hyperv_vm.Name -Path $win2022_iso_path }
    }
    if ($vm.type -eq "linux") {
        Set-VMFirmware -VMName $new_hyperv_vm.Name -EnableSecureBoot Off
    }
    $vm_dvd=Get-VMDvdDrive -VMName $new_hyperv_vm.Name
    Set-VMFirmware -VMName $new_hyperv_vm.Name -FirstBootDevice $vm_dvd
    Set-VM -Name $new_hyperv_vm.Name -ProcessorCount $vm.cpu
    if ($vm.name -eq $vm_gw_name) {
        Add-VMNetworkAdapter -VMName $vm.name -SwitchName $external_switch_name
    }
    Start-VM -Name $new_hyperv_vm.Name
}
