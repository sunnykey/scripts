#certificate location https://1drv.ms/u/s!ApGdpvDePy2XoFXaVGerM-G50_sL?e=LA7Zxf
#
#explaination https://techcommunity.microsoft.com/t5/azure-governance-and-management/azure-instance-metadata-service-attested-data-tls-critical/ba-p/2888953

$archive_url='<azure_url>'
$archive_path=$env:TEMP+'\Certs_azure.zip'
$extracted_files_path=$env:TEMP+'\certs'
$certificate_file_extension_filter="*.crt"
$certificate_store_location="Cert:\LocalMachine\TrustedPublisher"

Import-Module PKI
Import-Module Microsoft.Powershell.Archive

if (!(Test-Path $extracted_files_path)) {
    New-Item -Path $extracted_files_path -ItemType Directory -ErrorAction SilentlyContinue
}
Invoke-WebRequest -Uri $archive_url -OutFile $archive_path
Expand-Archive -Path $archive_path -DestinationPath $extracted_files_path -Force
Set-Location $extracted_files_path
Get-ChildItem -Name $certificate_file_extension_filter -File | ForEach-Object {
    Import-Certificate -FilePath $_ -CertStoreLocation $certificate_store_location -Confirm:$false
}
Set-Location $env:TEMP
Remove-Item -Path $archive_path -Force
Remove-Item -Path $extracted_files_path -Recurse -Force
