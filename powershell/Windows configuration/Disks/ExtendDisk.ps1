#this script extend volume on disk to maximum size
$volume_label='C'

$target_partition=Get-Partition -DriveLetter $volume_label
$recovery_partition=Get-Partition | Where-Object Type -eq Recovery

#if recovery partition is behind main partition than remove it
if ($recovery_partition.PartitionNumber -gt $target_partition.PartitionNumber) {
    #if you want to remove partition without confirmation, use -Confirm:$false
    Remove-Partition -PartitionNumber $recovery_partition.PartitionNumber -DiskNumber $recovery_partition.DiskNumber -Confirm -Verbose
}
$max_partition_size=Get-PartitionSupportedSize -PartitionNumber $target_partition.PartitionNumber -DiskNumber $target_partition.DiskNumber
Resize-Partition -PartitionNumber $target_partition.PartitionNumber -Size $max_partition_size.SizeMax -DiskNumber $target_partition.DiskNumber

