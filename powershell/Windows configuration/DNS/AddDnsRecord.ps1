param(
    [string]$record_type='CNAME',
    [string]$record_value='<DNS record value>',
    [string]$record_name='<DNS record name>',
    [string]$zone_name='<zone_name>'
)

Import-Module ActiveDirectory
if ($?) {
    $DNS_server=(Get-ADDomain).PDCEmulator
} else {
    Write-Host "Active Directory module for powershell was not found"
    return 2
}

Import-Module DNSServer
if ($?) {
    switch ($record_type.ToLower()) {
        'cname' {
            Add-DnsServerResourceRecordCName -HostNameAlias $record_value -Name $record_name -ComputerName $DNS_server -ZoneName $zone_name -Confirm:$false
        }
        default {
            Add-DnsServerResourceRecordA -CreatePtr -IPv4Address $record_value -Name $record_name -ZoneName $zone_name -ComputerName $DNS_server -Confirm:$false
        }
    }
} else {
    Write-Host "DNS Server module for powershell was not found"
    return 2
}
