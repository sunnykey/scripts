#this script updates Windows DNS server conditional forwarding

$forwarded_domain_list='azurewebsites.net','blob.core.windows.net','mysql.database.azure.com'
$azure_dns_address='168.63.129.16'
$cloud_dns_server_name_list='<cloud_dns_server1>','<cloud_dns_server1>'
$onprem_dns_server_name_list='<onprem_dns_server1>','<onprem_dns_server2>','<onprem_dns_server3>'

Import-Module -Name DnsServer
#update DNS servers in the cloud
foreach ($dns_server in $cloud_dns_server_name_list) {
    foreach ($domain in $forwarded_domain_list) {
        Remove-DnsServerZone -ComputerName $dns_server -Name $domain -Force
        if ($?) {
            Write-Warning "Removed zone $domain on $dns_server"
        }
        Add-DnsServerConditionalForwarderZone -Name $domain -MasterServers $azure_dns_address -ComputerName $dns_server
    }
}
#update on-prem DNS servers
$cloud_dns_server_addresses=@()
foreach ($name in $cloud_dns_server_name_list) {
    $cloud_dns_server_addresses+=(Resolve-DnsName $name -Type A).IpAddress
}
foreach ($dns_server in $onprem_dns_server_name_list) {
    foreach ($domain in $forwarded_domain_list) {
        Remove-DnsServerZone -ComputerName $dns_server -Name $domain -Force
        if ($?) {
            Write-Warning "Removed zone $domain on $dns_server"
        }
        Add-DnsServerConditionalForwarderZone -Name $domain -MasterServers $cloud_dns_server_addresses -ComputerName $dns_server      
    }
}
