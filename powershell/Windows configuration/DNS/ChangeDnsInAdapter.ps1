param(
    $old_dns_server_address='<old_ip>',
    $new_dns_server_address='<old_new>'
)

$adapter_name="Ethernet*"
$new_dns_server_address_list=@()
$adapter=Get-NetAdapter -Name $adapter_name
$dns_settings=$adapter | Get-DnsClientServerAddress -AddressFamily IPv4
foreach ($address in $dns_settings.ServerAddresses) {
    if ($address -eq $old_dns_server_address) {
        $address=$new_dns_server_address
    }
    $new_dns_server_address_list+=$address
}
$adapter | Set-DnsClientServerAddress -ServerAddresses $new_dns_server_address_list

