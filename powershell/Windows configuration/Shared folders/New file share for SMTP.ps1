$folder_name='PRTG'
$share_path='C:\inetpub\logs\'+$folder_name+'\'
$share_name=$folder_name+'_logs'
$computer_search_filter='*PRTG*'
$ACL_array=@()

Import-Module ActiveDirectory

Get-ADComputer -Filter {Name -like $computer_search_filter} | ForEach-Object {
    $ACL_array+=($_.Name+'$')
}
$ACL_array+='<AD group>'
$ACL_array+='<AD username>'
if (!(Test-Path $share_path)) {
    mkdir $share_path
}
New-SmbShare -FolderEnumerationMode AccessBased -ReadAccess $ACL_array -Path $share_path -Name $share_name
