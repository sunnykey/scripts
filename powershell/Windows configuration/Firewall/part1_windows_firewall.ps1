#source documents for this script
#https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-2000-server/bb727063(v=technet.10)?redirectedfrom=MSDN
#https://support.microsoft.com/en-us/help/179442/how-to-configure-a-firewall-for-domains-and-trusts


#set up incomming rules
#allow sync time with PDC emulator
New-NetFirewallRule -Name "allow w32time" -DisplayName "allow w32time" -Enabled True -Profile Any -Direction Inbound -Action Allow -Protocol UDP -LocalPort 123

#allow access to changing password with Kerberos service
#allow change password with TCP request
New-NetFirewallRule -Name "allowKerberos password change TCP" -DisplayName "allow Kerberos password change" -Enabled True -Profile Any -Direction Inbound -Action Allow -Protocol TCP -LocalPort 464
#allow change password with UDP request
New-NetFirewallRule -Name "allowKerberos password change UDP" -DisplayName "allow Kerberos password change" -Enabled True -Profile Any -Direction Inbound -Action Allow -Protocol UDP -LocalPort 464
#allow use Kerberos with TCP
New-NetFirewallRule -Name "Kerberos TCP" -DisplayName "Kerberos TCP" -Enabled True -Profile Any -Direction Inbound -Action Allow -Protocol TCP -LocalPort 88
#allow use Kerberos with UDP
New-NetFirewallRule -Name "Kerberos UDP" -DisplayName "Kerberos UDP" -Enabled True -Profile Any -Direction Inbound -Action Allow -Protocol UDP -LocalPort 88

#allow use RPC calls for various management componets
New-NetFirewallRule -Name "allow RPC Endpoint Mapper" -DisplayName "allow RPC Endpoint Mapper" -Enabled True -Profile Any -Direction Inbound -Action Allow -Protocol TCP -LocalPort 135
#allow use Dynamic ports for RPC calls
New-NetFirewallRule -Name "RPC for LSA, SAM, Netlogon TCP" -DisplayName "RPC for LSA, SAM, Netlogon TCP" -Enabled True -Profile Any -Direction Inbound -Action Allow -Protocol TCP -LocalPort 49152-65535

#allow connect to LDAP catalog
#allow to use LDAP by TCP
New-NetFirewallRule -Name "LDAP TCP" -DisplayName "LDAP TCP" -Enabled True -Profile Any -Direction Inbound -Action Allow -Protocol TCP -LocalPort 389
#allow to use LDAP by UDP
New-NetFirewallRule -Name "LDAP UDP" -DisplayName "LDAP UDP" -Enabled True -Profile Any -Direction Inbound -Action Allow -Protocol UDP -LocalPort 389
#allow to use encrrypted LDAP by TCP
New-NetFirewallRule -Name "LDAP SSL TCP" -DisplayName "LDAP SSL TCP" -Enabled True -Profile Any -Direction Inbound -Action Allow -Protocol TCP -LocalPort 636
#allow to use Global Catalog in Active Directory
New-NetFirewallRule -Name "LDAP GC" -DisplayName "LDAP GC" -Enabled True -Profile Any -Direction Inbound -Action Allow -Protocol TCP -LocalPort 3268,3269

#allow to use DNS Server
#allow tcp request
New-NetFirewallRule -Name "DNS client TCP" -DisplayName "DNS client TCP" -Enabled True -Profile Any -Direction Inbound -Action Allow -Protocol TCP -LocalPort 53
#allow udp request
New-NetFirewallRule -Name "DNS client UDP" -DisplayName "DNS client UDP" -Enabled True -Profile Any -Direction Inbound -Action Allow -Protocol UDP -LocalPort 53

#allow to use shared folders
New-NetFirewallRule -Name "SMB" -DisplayName "SMB" -Enabled True -Profile Any -Direction Inbound -Action Allow -Protocol TCP -LocalPort 445

#allow to test network activity
#allow ping with ipv4
New-NetFirewallRule -Name "allow ping v4 in" -DisplayName "allow ping v4 in" -Enabled True -Profile Any -Direction Inbound -Action Allow -IcmpType 0 -Protocol ICMPV4
#allow ping with ipv6
New-NetFirewallRule -Name "allow ping v6 in" -DisplayName "allow ping v6 in" -Enabled True -Profile Any -Direction Inbound -Action Allow -IcmpType 128 -Protocol ICMPV6

#set up outgoing rules
#allow outgoing reply for a Active Directory Service
New-NetFirewallRule -Name "Outgoing connections" -DisplayName "Outgoing connections" -Enabled True -Profile Any -Direction Outbound -Action Allow -Protocol TCP -RemotePort 53,49152-65535

#allow to reply for test network activity
#allow ping reply with ipv4
New-NetFirewallRule -Name "allow ping v4 out" -DisplayName "allow ping v4 out" -Enabled True -Profile Any -Direction Outbound -Action Allow -IcmpType 8 -Protocol ICMPV4
#allow ping reply with ipv6
New-NetFirewallRule -Name "allow ping v6 out" -DisplayName "allow ping v6 out" -Enabled True -Profile Any -Direction Outbound -Action Allow -IcmpType 129 -Protocol ICMPV6
