#set up domain controller address, for real test it should be change to real address
$domain_controller_address="dc1-ad2.itsp-vl.ru"

#define function for repeatable operation
function Check-RemotePorts($RemoteAddress,$RemotePort) {
    #check does remote port is open and make decision after test
    $result=Test-NetConnection -ComputerName $RemoteAddress -RemotePort $RemotePort
    if ($result.TcpTestSucceded) {
        #write green text in console if port is opened
        Write-Host -Object "Port $RemotePort is opened and command has this output" -ForegroundColor Green
        #write o
        Write-Host -Object $result
    } else {
        #write yellow text in console if port is closed
        Write-Host -Object "Port $RemotePort is closed" -ForegroundColor Yellow
    }
    
}
#check ldap port 389
Check-RemotePorts -RemoteAddress $domain_controller_address -RemotePort 389
#check ldap port 636
Check-RemotePorts -RemoteAddress $domain_controller_address -RemotePort 636
#check ldap port 3268
Check-RemotePorts -RemoteAddress $domain_controller_address -RemotePort 3268
#check ldap port 3269
Check-RemotePorts -RemoteAddress $domain_controller_address -RemotePort 3269
#check connection to AD, this command check ldap and dns connectivity, test pass if run from domain joined Windows machine
#we call classic batch command, suppres error output and then parse only normal output
$result=cmd /c "net user /domain 2> nul"
if (($result | Select-String -Pattern "The command completed successfull")) {
    #write green text in console if port is opened
    Write-Host -Object "Retrieving users from domain was successful and command has this output" -ForegroundColor Green
    Write-Host -Object $result
} else {
    #write yellow text in console if port is closed
    Write-Host -Object "Retrieving users from domain was failed" -ForegroundColor Yellow
}
#check Kerberos port 88
Check-RemotePorts -RemoteAddress $domain_controller_address -RemotePort 88

#Kerberos password change, port 464
Check-RemotePorts -RemoteAddress $domain_controller_address -RemotePort 464

#check SMB connectivity, port 445
Check-RemotePorts -RemoteAddress $domain_controller_address -RemotePort 445
#try to browse network folder via SMB, test pass if run from domain joined Windows machine
#we call classic batch command, suppres error output and then parse only normal output
$result=cmd /c "net view \\$domain_controller_address /all 2> nul"
#if result has output print then test succeeded
if (($result | Select-String -Pattern "Disk")) {
    #write green text in console if port is opened
    Write-Host -Object "Test network share was successful and command has this output" -ForegroundColor Green
    Write-Host -Object $result
} else {
    #write yellow text in console if port is closed
    Write-Host -Object "Test network share was failed" -ForegroundColor Yellow
}
#check DNS server with TCP request
$result=Resolve-DnsName -Name "google.com" -TcpOnly -Server $domain_controller_address
if ($result) {
        #write green text in console if test was succeeded
        Write-Host -Object "TCP DNS request was successful and it has output" -ForegroundColor Green
        #show test command output
        $result | Format-Table
    } else {
        #write yellow text in console if test was failed
        Write-Host -Object "TCP DNS request was falied" -ForegroundColor Yellow
    }
#check DNS server with UDP request
$result=Resolve-DnsName -Name "google.com" -Server $domain_controller_address
if ($result) {
        #write green text in console if test was succeeded
        Write-Host -Object "UDP DNS request was successful and it has output" -ForegroundColor Green
        #show test command output
        $result | Format-Table
    } else {
        #write yellow text in console if test was failed
        Write-Host -Object "UDP DNS request was falied" -ForegroundColor Yellow
    }


#test ICMP reply
if (Test-Connection $domain_controller_address -Quiet) {
    Write-Host "ICMP port is opened" -ForegroundColor Green
} else {
    Write-Host "ICMP port is opened" -ForegroundColor Green
}