$port=3389
$VPN_local_address='<ip_address>'
$output_csv_name='Results.csv'
$server_address_file='serverAddresses.csv'

#csv format Name,publicIP,privateIP
$server_addresses_csv_path=(Split-Path ($MyInvocation.MyCommand.Path) -Parent)+'\'+$server_address_file
$full_output_csv_path=(Split-Path ($MyInvocation.MyCommand.Path) -Parent)+'\'+$output_csv_name
$host_test_results=''

$csv=Get-Content -Path $server_addresses_csv_path | ConvertFrom-Csv
'HostName,PublicIp,IsPortOpenViaPublicIP,RoutingPublicIP,PrivateIp,IsPortOpenViaPrivateIP,RoutingPrivateIP' | Out-File -FilePath $full_output_csv_path -Encoding utf8
foreach ($row in $csv) {
    Write-Host ("Test public connectivity for host "+$row.Name)
    $host_test_results=$row.Name+','+$row.PublicIp+','
    if (Test-NetConnection $row.publicIP -Port $port -InformationLevel Quiet -WarningAction SilentlyContinue) {
        $host_test_results+='Yes,'
    } else {
        $host_test_results+='No,'
    }
    if ((Test-NetConnection $row.PublicIp -TraceRoute -Hops 1 -WarningAction SilentlyContinue).TraceRoute -eq $VPN_local_address) {
        $host_test_results+='Routed,'
    } else {
        $host_test_results+='Not routed,'
    }
    
    Write-Host ("Test private connectivity for host "+$row.Name)
    $host_test_results+=$row.PrivateIp+','
    if (Test-NetConnection $row.privateIP -Port $port -InformationLevel Quiet -WarningAction SilentlyContinue) {
        $host_test_results+='Yes,'
    } else {
        $host_test_results+='No,'
    }
    if ((Test-NetConnection $row.PrivateIp -TraceRoute -Hops 1 -WarningAction SilentlyContinue).TraceRoute -eq $VPN_local_address) {
        $host_test_results+='Routed'
    } else {
        $host_test_results+='Not routed'
    }
    $host_test_results | Out-File -FilePath $full_output_csv_path -Encoding utf8 -Append
}
