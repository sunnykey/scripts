param (
    [parameter(Position=0,Mandatory=$true)]$config="./ConfigDev.json",
    [parameter(Position=1)]$operation="downloading" #downloading or uploading
)

$operation=([string]$operation).ToLower() #modifying operation switch to lower character text
Write-Host "Log path in the function is $log_path" -ForegroundColor Yellow
#read config
$json=Get-Content $config | ConvertFrom-Json

$remote_username=$json.credentials.username
$remote_password=$json.credentials.password
$ssh_key_path=$json.credentials.ssh_key_path

$remote_server_address=$json.remote_host.address
$remote_server_port=$json.remote_host.port
$remote_server_path=$json.remote_host.path
$remote_server_filter=$json.remote_host.filter

$proxy_server_address=$json.proxy.address
$proxy_server_port=$json.proxy.port

$local_folder_path=$json.script_settings.local_path
$psftp_file_path='"'+$json.script_settings.psftp.path+'"'
$psftp_config_file=$json.script_settings.psftp.temp_config_path
$putty_profile_name=$json.script_settings.psftp.profile_name
$registry_settings=$json.script_settings.psftp.registry_settings_path
$registry_branch="HKCU:\SOFTWARE\SimonTatham\PuTTY\Sessions\"+$putty_profile_name #putty settings location in registry

$logging_enabled=$json.logging.enabled
$log_path=$json.logging.path

$arching_enabled=$json.archiving.enabled
$archiving_folder_path=$json.archiving.path
$archiving_retention_days_count=0-[Int32]$json.archiving.retention

#starting log writting
if ($logging_enabled) {
    "------------------------------------" | Out-File -FilePath $log_path -Append -Encoding ascii
    ("Log started at "+(Get-Date)) | Out-File -FilePath $log_path -Append -Encoding ascii
}

#check if registry file exists
if (!(Test-Path $registry_settings)) {
    Write-Host "Registry file was not found. Check your config file"
    "Registry file was not found. Check your config file" | Out-File -FilePath $log_path -Append -Encoding ascii
    exit 14002
}

#building putty settings in registry
Copy-Item $registry_settings ($registry_settings+'1') -Force
reg.exe import $registry_settings
Set-ItemProperty -Path $registry_branch -Name "HostName" -Value $remote_server_address -Force
Set-ItemProperty -Path $registry_branch -Name "PortNumber" -Value $remote_server_port -Force
Set-ItemProperty -Path $registry_branch -Name "ProxyHost" -Value $proxy_server_address -Force
Set-ItemProperty -Path $registry_branch -Name "ProxyPort" -Value $proxy_server_port -Force

if ($ssh_key_path -ne "") {
    #building argument list with ssh key file
    $pstfp_command_argumets="-load $putty_profile_name -b $psftp_config_file -batch -bc -be -v -l $remote_username -i $ssh_key_path" #arguments for psftp.exe
} else {
    #password modification if escape symbols exist
    $remote_password=$remote_password.Replace("^","^^")
    $remote_password=$remote_password.Replace("&","^&")
    $remote_password=$remote_password.Replace("<","^<")
    $remote_password=$remote_password.Replace("\","^\")
    $remote_password=$remote_password.Replace(">","^>")
    $remote_password=$remote_password.Replace("|","^|")
    #build argument list with password
    $pstfp_command_argumets="-load $putty_profile_name -b $psftp_config_file -batch -bc -be -v -l $remote_username -pw `"$remote_password`"" #arguments for psftp.exe
}

#if file filter is not defined then select all files
if (!($remote_server_filter)) {
    $remote_server_filter='*'
}
#build psftp config
"Create psftp config file"
"Create psftp config file $psftp_config_file" | Out-File -FilePath $log_path -Append -Encoding ascii
if ($operation -eq "uploading") {
    "cd $remote_server_path" | Out-File -FilePath $psftp_config_file -Encoding ascii
    "mput $remote_server_filter" | Out-File -FilePath $psftp_config_file -Encoding ascii -Append
    "bye" | Out-File -FilePath $psftp_config_file -Encoding ascii -Append
} else {
    "cd $remote_server_path" | Out-File -FilePath $psftp_config_file -Encoding ascii
    "mget $remote_server_filter" | Out-File -FilePath $psftp_config_file -Encoding ascii -Append
    "bye" | Out-File -FilePath $psftp_config_file -Encoding ascii -Append
}

#creating archive folder if it doesn't exist
"Create archive folder"
if ($arching_enabled) {
    if (!(Test-Path $archiving_folder_path)) {
        "Create archive folder $archiving_folder_path" | Out-File -FilePath $log_path -Append -Encoding ascii
        New-Item -Path $archiving_folder_path -ItemType Directory
        if ($?) {
            Write-Host "Can't create archive folder"
            "Can't create archive folder" | Out-File -FilePath $log_path -Append -Encoding ascii
            exit 14003
        }
    }
}

Set-Location $local_folder_path
if (($operation -eq "downloading") -and ($arching_enabled)) {
    "Archiving files before downloading"
    if ($logging_enabled) {
        "Moved files to archive:" | Out-File -FilePath $log_path -Append -Encoding ascii
        $file_list=Get-ChildItem -Path "*"
        if ($file_list) {
            $file_list | ForEach-Object {Tee-Object -InputObject $_ -FilePath $log_path -Append | Move-Item -Destination $archiving_folder_path -Force}
        } else {
            "No files to archive in folder $local_folder_path" | Out-File -FilePath $log_path -Append -Encoding ascii
        }
    } else {
        Get-ChildItem -Path "*" | Move-Item -Destination $archiving_folder_path -Force
    }
}
"Run the psftp: $psftp_file_path $pstfp_command_argumets"
Invoke-Expression "cmd.exe /c chcp 1251 & $psftp_file_path $pstfp_command_argumets >> $log_path" #copy files to or from remote host via psftp
if ($operation -eq "downloading") {
    "List downloaded files:" | Out-File -FilePath $log_path -Append -Encoding ascii
} else {
    "List uploaded files:" | Out-File -FilePath $log_path -Append -Encoding ascii
}
#logging copied files list
$file_list=Get-ChildItem -Path "*"
if ($file_list) {
    $file_list | Out-File -FilePath $log_path -Append
} else {
    "No files to transfer in folder $local_folder_path" | Out-File -FilePath $log_path -Append -Encoding ascii
}

if (($operation -eq "uploading") -and ($arching_enabled)) {
    "Archive files after uploading"
    if ($logging_enabled) {
        "Moved files to archive:" | Out-File -FilePath $log_path -Append -Encoding ascii
        $file_list=Get-ChildItem -Path "*"
        if ($file_list) {
            $file_list | ForEach-Object {Tee-Object -InputObject $_ -FilePath $log_path -Append | Move-Item -Destination $archiving_folder_path -Force}
        } else {
            "No files to archive in folder $local_folder_path" | Out-File -FilePath $log_path -Append -Encoding ascii
        }
    } else {
        Get-ChildItem -Path "*" | Move-Item -Destination $archiving_folder_path -Force
    }
}
"Remove temporary files"
"Remove psftp config file $psftp_config_file" | Out-File -FilePath $log_path -Append -Encoding ascii
Remove-Item $psftp_config_file -Force

#clean up old files in archive folder
if ($arching_enabled) {
    Set-Location $archiving_folder_path
    "Remove files below from archive" | Out-File -FilePath $log_path -Append -Encoding ascii
    $file_list=Get-ChildItem -Path "*" | Where-Object LastWriteTime -lt ((Get-Date).AddDays($archiving_retention_days_count))
    if ($file_list) {
        $file_list | ForEach-Object {Tee-Object -InputObject $_ -FilePath $log_path -Append | Remove-Item -Force -Recurse -Confirm:$false}
    } else {
        "No files to remove in folder $archiving_folder_path" | Out-File -FilePath $log_path -Append -Encoding ascii
    }
}

#finishing log writting
if ($logging_enabled) {
    ("Log ended at "+(Get-Date)) | Out-File -FilePath $log_path -Append -Encoding ascii
}
Move-Item ($registry_settings+'1') $registry_settings -Force