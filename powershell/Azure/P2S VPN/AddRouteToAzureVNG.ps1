$subcription_name='<subcription_name>'
$VNG_name_list='<Virtual_network_gateway1>','<Virtual_network_gateway1>'
$resource_group_name='RG-EA-PROD-NETWORKS'
$route_list='192.168.0.0/16', '172.16.0.0/12', '10.0.0.0/8'

Get-AzSubscription -SubscriptionName $subcription_name | Set-AzContext
foreach ($VNG in $VNG_name_list) {
    $gw = Get-AzVirtualNetworkGateway -Name $VNG -ResourceGroupName $resource_group_name
    Set-AzVirtualNetworkGateway -VirtualNetworkGateway $gw -CustomRoute $route_list
}
