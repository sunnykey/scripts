﻿Import-Module Microsoft.PowerShell.Utility

#$RSS_feed_url="https://www.globenewswire.com/RssFeed/orgclass/1"
$RSS_feed_url="https://www.globenewswire.com/RssFeed/country/United%20States"
$token='ddfghd89mm234234vbn'
$webhook_site="https://example.com"
#change domain.com for real domain name
$result_website="https://hooks.example.com/hooks/catch/2308014/orrlf4a/?" 
##
$rss_title_filter=@('Order','increase','positive','Receive','Commercial','awarded','Win','contract')
#$rss_title_filter=@('Order','director')
$saved_rss_title_list='~\Documents\скрипты\titleDB.txt'
#$saved_rss_title_list=$PSScriptRoot+'\titleDB.txt'


$saved_rss_title_limit_strings=25
$saved_rss_title_list
#parse rss and save values which match NASDAQ or NYSE and doesn't empty
while ($true) {
    $request=Invoke-WebRequest -Uri $RSS_feed_url
    $Rss_feed=([xml]($request.Content)).rss.channel
    $RSS_all_records=@()
    $saved_rss_title_list_content=@()
    foreach ($RSS_msg in $Rss_feed.Item) {
        if ($RSS_msg.Category -ne $null) {
            $RSS_category_node=$RSS_msg.Category.GetValue(0).'#text'
            if (($RSS_category_node -like "*NASDAQ*") -or ($RSS_category_node -like "*NYSE*")) {
                $saved_rss_title_list_content=Get-Content $saved_rss_title_list -Encoding UTF8 -ErrorAction SilentlyContinue
                foreach ($f in $rss_title_filter) {
                    if ($RSS_msg.Title -match $f) {
                        if ($saved_rss_title_list_content -eq $null) {
                            $saved_rss_title_list_content="1"
                        }
                        $is_string_present_in_file=$false
                        foreach ($str in $saved_rss_title_list_content) {
                            if ($RSS_msg.Title -eq $str) {
                                $is_string_present_in_file=$true
                                break
                            }
                        }
                        if (!($is_string_present_in_file)) {
                            Write-Host ("Matched title is \"+$RSS_msg.title+'\') -ForegroundColor Green
                            $RSS_all_records+=[PSCustomObject]@{
                                Link=$RSS_msg.Link
                                Title=$RSS_msg.Title
                                Date=[datetime]$RSS_msg.pubDate
                                Publisher=$RSS_msg.publisher
                                Category=$RSS_category_node
                            }
                            $RSS_msg.Title | Out-File -FilePath $saved_rss_title_list -Encoding utf8 -Append
                        } else {Write-Host ("Title \"+$RSS_msg.title+'\ already present in file') -ForegroundColor Magenta}
                        break
                    } #else {Write-Host ("The title \"+$RSS_msg.title+"\ doesn't match key word $f") -ForegroundColor Yellow}
                }
            }   
        }
    }
    "Got "+$RSS_all_records.Length+" new record(s)"
    foreach ($ticker in $RSS_all_records.Category) {
        $symbol=($ticker -split ':')[1]
        $stock_exchange_name=($ticker -split ':')[0]
        switch ($stock_exchange_name) {
            "NYSE" {$url_ticker=$stock_exchange_name}
            "Nasdaq" {$url_ticker="island"}
        }
        "Stock is "+$stock_exchange_name
        $webhook_url=$webhook_site+'/api/v1/quote?symbol='+$symbol+'&token='+$token
        "Webhook URL is "+$webhook_url
        $request=Invoke-WebRequest -Uri $webhook_url
        $HttpResponse=$request.Content
        "Http responce is "+$HttpResponse
        $str=($HttpResponse -split ',') | Select-String "pc"
        $price_close=[float](($str -split ':')[1])
        $volume=[Math]::Round((1000/($price_close*1.25)),0)
        $desired_maximum_price=[Math]::Round((1.05*$price_close),2)
        "Volume is "+$volume
        "Max price is "+$desired_maximum_price
        $result_url=$result_website+'exchange='+$url_ticker+'&ticker='+$symbol+'&currency=usd&volume='+$volume+'&direction=buy&price='+$desired_maximum_price
        "Result URL is "+$result_url
        #Invoke-WebRequest -Uri $result_url
    }

    $saved_rss_title_list_content=@()
    $saved_rss_title_list_content=Get-Content $saved_rss_title_list -Encoding UTF8 -ErrorAction SilentlyContinue
    if ($saved_rss_title_list_content.Length -gt $saved_rss_title_limit_strings) {
        $saved_rss_title_list_content | Select-Object -Last $saved_rss_title_limit_strings | Out-File -FilePath ($saved_rss_title_list+'1') -Encoding utf8 -Append
        Move-Item ($saved_rss_title_list+'1') $saved_rss_title_list -Force
    }
    Start-Sleep -Seconds 10
}