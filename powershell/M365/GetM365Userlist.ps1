#Requires -Version 5.1
#Requires -PSEdition Desktop
#Requires -RunAsAdministrator

$module_name='MSOnline'

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12;
Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force;
Set-PSRepository -Name PSGallery -InstallationPolicy Trusted;
Install-Module -Name $module_name -Force #-AllowPrerelease
Import-Module $module_name
Connect-MsolService
Get-MsolUser