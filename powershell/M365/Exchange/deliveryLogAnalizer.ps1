param(
    $CsvReportPath="~/Downloads/MessageTraceResult (3).csv"
)

$csv=Import-Csv -Path $CsvReportPath
$emails=$csv | Group-Object -Property RecipientAddress
foreach ($email in $emails.Name) {
    $failed_delivery=0
    $success_delivery=0
    $results=$csv | Where-Object RecipientAddress -eq $email | Select-Object -Property Received,RecipientAddress,Status,Subject |`
        Group-Object -Property Status
    $failed_delivery=($results | Where-Object Name -eq "Failed").Count
    $success_delivery=($results | Where-Object Name -eq "Delivered").Count
    $output_string="There were "+($failed_delivery+$success_delivery)+" attempts for address $email, successfull emails: $success_delivery, failed emails: $failed_delivery"
    $output_string
}
