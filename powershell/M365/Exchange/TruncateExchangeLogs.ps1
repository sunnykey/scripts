$dbname="db2019-1"
$edb_path=(Get-MailboxDatabase $dbname).EdbFilePath
Set-MailboxDatabase $dbname -CircularLoggingEnabled $true
Dismount-Database $dbname -Confirm:$false
Set-Location (Split-Path -Path $edb_path -Parent)
if ((eseutil /mh (".\"+$dbname+".edb") | Select-String -Pattern "Clean Shutdown")) {
    Remove-Item *.log
    Mount-Database $dbname
}

