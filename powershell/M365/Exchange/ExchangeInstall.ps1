#https://docs.microsoft.com/en-us/exchange/plan-and-deploy/deploy-new-installations/unattended-installs?view=exchserver-2019
Stop-Service MBAMService
.\setup.exe /IAcceptExchangeServerLicenseTerms_DiagnosticDataON /ps
.\setup.exe /IAcceptExchangeServerLicenseTerms_DiagnosticDataON /p
.\setup.exe /IAcceptExchangeServerLicenseTerms_DiagnosticDataON /pad
.\setup.exe /IAcceptExchangeServerLicenseTerms_DiagnosticDataON /m:Upgrade

.\setup.exe /IAcceptExchangeServerLicenseTerms_DiagnosticDataON /m:Install /r:mb

#old installation
.\setup.exe /IAcceptExchangeServerLicenseTerms /ps
.\setup.exe /IAcceptExchangeServerLicenseTerms /p
.\setup.exe /IAcceptExchangeServerLicenseTerms /pad

setup.exe /IAcceptExchangeServerLicenseTerms /m:Install /r:mb

#2013 .\setup.exe /IAcceptExchangeServerLicenseTerms /m:Install /r:CA,MB
.\setup.exe /IAcceptExchangeServerLicenseTerms /m:Upgrade

.\setup.exe /IAcceptExchangeServerLicenseTerms /m:Uninstall

#installation https://docs.microsoft.com/en-us/exchange/plan-and-deploy/prerequisites?view=exchserver-2019
Install-WindowsFeature Server-Media-Foundation, NET-Framework-45-Features, RPC-over-HTTP-proxy, RSAT-Clustering, RSAT-Clustering-CmdInterface, RSAT-Clustering-Mgmt, RSAT-Clustering-PowerShell, WAS-Process-Model, Web-Asp-Net45, Web-Basic-Auth, Web-Client-Auth, Web-Digest-Auth, Web-Dir-Browsing, Web-Dyn-Compression, Web-Http-Errors, Web-Http-Logging, Web-Http-Redirect, Web-Http-Tracing, Web-ISAPI-Ext, Web-ISAPI-Filter, Web-Lgcy-Mgmt-Console, Web-Metabase, Web-Mgmt-Console, Web-Mgmt-Service, Web-Net-Ext45, Web-Request-Monitor, Web-Server, Web-Stat-Compression, Web-Static-Content, Web-Windows-Auth, Web-WMI, Windows-Identity-Foundation, RSAT-ADDS

# Virtual directory	Internal URL value
# Autodiscover	No internal URL displayed
# ECP	https://owa.contoso.com/ecp
# EWS	https://mail.contoso.com/EWS/Exchange.asmx
# Microsoft-Server-ActiveSync	https://mail.contoso.com/Microsoft-Server-ActiveSync
# OAB	https://mail.contoso.com/OAB
# OWA	https://owa.contoso.com/owa
# PowerShell	http://mail.contoso.com/PowerShell
$domain='<domain_name>'
$ecp="https://"+$domain+"/ecp"
$ews="https://"+$domain+"/EWS/Exchange.asmx"
$active_sync="https://"+$domain+"/Microsoft-Server-ActiveSync"
$oab="https://"+$domain+"/OAB"
$owa="https://"+$domain+"/owa"
$pwsh="https://"+$domain+"/PowerShell"
$mapi="https://"+$domain+'/mapi'
$autodiscover="https://"+$domain+'/Autodiscover/Autodiscover.xml'
Get-EcpVirtualDirectory -Server $env:COMPUTERNAME | Set-EcpVirtualDirectory -InternalUrl $ecp -ExternalUrl $ecp
Get-WebServicesVirtualDirectory -Server $env:COMPUTERNAME | Set-WebServicesVirtualDirectory -InternalUrl $ews -ExternalUrl $ews
Get-ActiveSyncVirtualDirectory -Server $env:COMPUTERNAME | Set-ActiveSyncVirtualDirectory -InternalUrl $active_sync -ExternalUrl $active_sync
Get-OabVirtualDirectory -Server $env:COMPUTERNAME | Set-OabVirtualDirectory -InternalUrl $oab -ExternalUrl $oab
Get-OwaVirtualDirectory -Server $env:COMPUTERNAME | Set-OwaVirtualDirectory -InternalUrl $owa -ExternalUrl $owa
Get-PowerShellVirtualDirectory -Server $env:COMPUTERNAME | Set-PowerShellVirtualDirectory -InternalUrl $pwsh -ExternalUrl $pwsh
Get-MapiVirtualDirectory -Server $env:COMPUTERNAME | Set-MapiVirtualDirectory -InternalUrl $mapi -IISAuthenticationMethods Negotiate -ExternalUrl $mapi
Set-OrganizationConfig -MapiHttpEnabled $true
Get-OutlookAnywhere -Server $env:COMPUTERNAME | Set-OutlookAnywhere -InternalHostname $domain `
    -ExternalHostname $domain -ExternalClientsRequireSsl $true -ExternalClientAuthenticationMethod NTLM `
    -InternalClientAuthenticationMethod NTLM -InternalClientsRequireSsl $true
Get-ClientAccessService -Identity $env:COMPUTERNAME | Set-ClientAccessServer -AutoDiscoverServiceInternalUri $autodiscover
iisreset

$old_db='<old_dbname>'
$new_db='<new_dbname>'
Get-MailboxDatabase -Server $env:COMPUTERNAME | Set-MailboxDatabase -Name $new_db -ProhibitSendReceiveQuota Unlimited `
    -ProhibitSendQuota Unlimited -CircularLoggingEnabled $true
Dismount-Database -Identity $new_db -Confirm:$false
Mount-Database -Identity $new_db
Get-Mailbox -Database $old_db | New-MoveRequest -TargetDatabase $new_db
Get-Mailbox -Database $old_db -Archive | New-MoveRequest -TargetDatabase $new_db
Get-Mailbox -Database $old_db -Monitoring | New-MoveRequest -TargetDatabase $new_db
Get-Mailbox -Database $old_db -Arbitration | New-MoveRequest -TargetDatabase $new_db
Get-Mailbox -Database $old_db -PublicFolder | New-MoveRequest -TargetDatabase $new_db

https://techcommunity.microsoft.com/t5/exchange-team-blog/best-practices-for-migrating-from-exchange-server-2013-to/ba-p/3773084
