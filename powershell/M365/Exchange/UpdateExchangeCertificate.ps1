# $pfx_path='C:\cert\mail_stdwi_com.pfx'
# $mypwd = ConvertTo-SecureString -String "1234" -Force -AsPlainText
# $cert=Get-ChildItem Cert:\LocalMachine\WebHosting\ | Where-Object NotBefore -gt (Get-Date).AddMonths(-2) | Sort NotBefore -Descending | Select-Object -First 1
# $cert | Export-PfxCertificate -FilePath $pfx_path -Password $mypwd
# Import-ExchangeCertificate -FileName ('\\'+$env:ComputerName+(Split-Path $pfx_path -NoQualifier)) -Password $mypwd -Server $env:ComputerName
# Enable-ExchangeCertificate -Thumbprint $cert.Thumbprint -Services SMTP,IIS -Confirm:$false
$LetsEncryptFolder="C:\win-acme\"
$WinACMEFile=$LetsEncryptFolder+"wacs.exe"

$DefaulServerName='mail.stdwi.com'
$DomainList="$DefaulServerName,autodiscover.stdwi.com"
#$DomainList+=($env:COMPUTERNAME).ToLower()+'.'+($env:USERDNSDOMAIN).ToLower()

$param='--target manual '
$param+="--host $DomainList "
$param+="--certificatestore My "
$param+='--installation iis,script '
$param+='--installationsiteid 1 '
$param+='--script "./Scripts/ImportExchange.ps1" '
$param+="--scriptparameters `"'{CertThumbprint}' 'IIS,SMTP,IMAP' 1 '{CacheFile}' '{CachePassword}' '{CertFriendlyName}'`""

Set-Location $LetsEncryptFolder
Start-Process -FilePath $WinACMEFile -ArgumentList $param -NoNewWindow -Wait
