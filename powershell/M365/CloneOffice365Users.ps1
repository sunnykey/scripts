#Requires -Version 5.1
#Requires -PSEdition Desktop
#Requires -RunAsAdministrator

$module_names='MSOnline','ExchangeOnlineManagement'
$surname='<surname>'
$name='<name>'
$email='<user>@example.com'
$title='<title>'
$user_clone_from='<Display name>'
$cleared_password=''

$display_name=$name+' '+$surname
#$password=ConvertTo-SecureString -String $cleared_password -Force -AsPlainText
$password=$cleared_password

foreach ($module in $module_names) {
    if (!(Get-Module -Name $module)) {
        [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12;
        Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force;
        Set-PSRepository -Name PSGallery -InstallationPolicy Trusted;
        Install-Module -Name $module -Force #-AllowPrerelease
        Import-Module $module
    }
}

Connect-MsolService
Connect-ExchangeOnline

$new_user=New-MSolUser -UserPrincipalName $email `
    -DisplayName $display_name `
    -FirstName $name `
    -LastName $surname `
    -Title $title `
    -Password $password `
    -ForceChangePassword $true
$clone=Get-MsolUser -SearchString $user_clone_from
Get-MsolGroup -ErrorAction SilentlyContinue | ForEach-Object {
    if (Get-MsolGroupMember -GroupObjectId $_.ObjectId | Where-Object EmailAddress -eq $clone.UserPrincipalName) {
        Write-Host ("Adding user "+$new_user.UserPrincipalName+" to group "+$_.DisplayName)
        #Add-MSolGroupMember -GroupObjectId $_.ObjectId -GroupMemberObjectId $new_user.ObjectId
        Add-UnifiedGroupLinks -Identity $_.DisplayName -LinkType Members -Links $new_user.UserPrincipalName
    }
}

Disconnect-MsolService
Disconnect-ExchangeOnline
