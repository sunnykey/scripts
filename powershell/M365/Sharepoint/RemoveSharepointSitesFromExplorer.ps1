#unlink account from Onedrive settings
$onedrive_settings_location="HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Desktop\NameSpace"
Get-ChildItem $onedrive_settings_location | ForEach-Object {
   Remove-Item -Path ($onedrive_settings_location+'\'+$_.PSChildName) -Force -Recurse
}

#logoff