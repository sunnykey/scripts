#Parameters for Processing
$site_name='8_FORUMMESSERLI'
$sharepoint_theme_name="Crazy Cyan"
#user sharepoint theme generator https://fluentuipr.z22.web.core.windows.net/heads/master/theming-designer/index.html
$sharepoint_theme=@{
    "themePrimary" = "#0ed400";
    "themeLighterAlt" = "#f4fdf3";
    "themeLighter" = "#d3f8d0";
    "themeLight" = "#aef2a9";
    "themeTertiary" = "#65e55c";
    "themeSecondary" = "#27d91a";
    "themeDarkAlt" = "#0dbe00";
    "themeDark" = "#0ba100";
    "themeDarker" = "#087700";
    "neutralLighterAlt" = "#faf9f8";
    "neutralLighter" = "#f3f2f1";
    "neutralLight" = "#edebe9";
    "neutralQuaternaryAlt" = "#e1dfdd";
    "neutralQuaternary" = "#d0d0d0";
    "neutralTertiaryAlt" = "#c8c6c4";
    "neutralTertiary" = "#e3cab0";
    "neutralSecondary" = "#c89a6d";
    "neutralSecondaryAlt" = "#c89a6d";
    "neutralPrimaryAlt" = "#ae743a";
    "neutralPrimary" = "#a36527";
    "neutralDark" = "#7c4d1e";
    "black" = "#5b3916";
    "white" = "#ffffff";
    }

Import-Module -Name Microsoft.Online.SharePoint.PowerShell -ErrorAction SilentlyContinue
If (!($?)) {
    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
    Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force
    Set-PSRepository -Name PSGallery -InstallationPolicy Trusted
    Install-Module -Name Microsoft.Online.SharePoint.PowerShell
    Import-Module -Name Microsoft.Online.SharePoint.PowerShell
}


$sharepoint_base_url = "messerliservices"
$sharepoint_admin_url='https://'+$sharepoint_base_url+'-admin.sharepoint.com'
$sharepoint_url='https://'+$sharepoint_base_url+'.sharepoint.com'
$full_site_name=$sharepoint_url+'/sites/'+$site_name

Connect-SPOService -Url $sharepoint_admin_url -ModernAuth $true
Add-SPOTheme -Identity $sharepoint_theme_name -Palette $sharepoint_theme -IsInverted $false
Set-SPOWebTheme -Theme $sharepoint_theme_name -Web $full_site_name
Disconnect-SPOService
