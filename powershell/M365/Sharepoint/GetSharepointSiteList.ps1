#https://pnp.github.io/powershell/articles/installation.html
# Install-Module -Name "PnP.PowerShell" -AllowPrerelease

# Register-PnPManagementShellAccess
#without MFA
#Connect-PnPOnline -Url https://[yourtenant].sharepoint.com -Credentials (Get-Credential)
# Connect-PnPOnline -Url https://organizationname-admin.sharepoint.com/ -Interactive
# Connect-PnPOnline -Url $sharepoint_site_url -UseWebLogin
#Requires -Version 5.1
#Requires -PSEdition Desktop
#Requires -RunAsAdministrator
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12;
Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force;
Set-PSRepository -Name PSGallery -InstallationPolicy Trusted;
Install-Module -Name Microsoft.Online.SharePoint.PowerShell -Force
Import-Module Microsoft.Online.SharePoint.PowerShell
$organization_name='organizationname'
$sharepoint_admin_site_url="https://"+$organization_name+"-admin.sharepoint.com/"
#$sharepoint_site_url="https://"+$organization_name+".sharepoint.com/"
Connect-SPOService -Url $sharepoint_admin_site_url
Get-SPOSite

