$OU_path="OU=Users,DC=example,DC=com"
$TO_login="<AD_login>"
$Admin_group="Administrators"
#input password here
$password="<some_password_here>"
#
$log_folder="c:\tmp"
$log_file=$log_folder+'\reset_login.log'

$secured_password=ConvertTo-SecureString -String $password -AsPlainText -Force
if (!(Test-Path $log_folder)) {
    mkdir $log_folder
}
if (Test-Path $log_file) {
    Remove-Item $log_file -Force
}
Get-ADComputer -Filter {Enabled -eq $true} -SearchBase $OU_path | ForEach-Object {
    $out_string="Reset password on host "+$_.Name
    Write-Host $out_string -ForegroundColor Yellow
    Out-File -FilePath $log_file -InputObject $out_string -Append -Encoding utf8
    $result=Invoke-Command -ComputerName $_.Name {
        #net user $TO_login $password /y
        if (Get-LocalUser -Name $using:TO_login -ErrorAction SilentlyContinue) {
            Set-LocalUser -Name $using:TO_login -Password $using:secured_password -Confirm:$false
            return ("Password for user "+$using:TO_login+" was reset")
        } else {
            New-LocalUser -Name $using:TO_login -Password $using:secured_password -AccountNeverExpires
            Add-LocalGroupMember -Group $using:Admin_group -Member $using:TO_login
            return ("User "+$using:TO_login+" was added")
        }        
    }
    Out-File -FilePath $log_file -InputObject $result -Append -Encoding utf8
}
