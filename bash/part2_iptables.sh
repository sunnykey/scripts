#!/bin/bash
#source about firewall ports
#https://docs.gitlab.com/omnibus/package-information/defaults.html
#https://about.gitlab.com/blog/2016/02/18/gitlab-dot-com-now-supports-an-alternate-git-plus-ssh-port/
#https://about.gitlab.com/install/#centos-8

#disable firewalld in CentOS8
#stop firewalld daemon which conficts with iptables
systemctl stop firewalld
#restartict firewalld start when system boot
systemctl disable firewalld
#install and enable iptables
yum install -y iptables iptables-services
#allow iptables start when system boot
systemctl enable iptables
# set up ingress connection
#clear all incomming rules
iptables -F INPUT
#allow ssh
iptables -A INPUT -p TCP --dport 22 -m comment --comment "SSH" -j ACCEPT
#allow connect to git in browser
iptables -A INPUT -p TCP --dport 80 -m comment --comment "gitlab_http" -j ACCEPT
#allow connect to git in browser with ssl
iptables -A INPUT -p TCP --dport 443 -m comment --comment "gitlab_https" -j ACCEPT
#allow git protocol
iptables -A INPUT -p TCP --dport 9418 -m comment --comment "gitlab_https" -j ACCEPT
#allow ping request
iptables -A INPUT -p ICMP --icmp-type 8 -m comment --comment "ping request" -j ACCEPT
#allow any trafic come if outgoing connection was established
iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
#default rule which drops other incomming connection
iptables -P INPUT DROP

#set up egress connections
#clear all outgoing rules
iptables -F OUTPUT
#allow ssh
iptables -A OUTPUT -p TCP --sport 22 -m comment --comment "SSH" -j ACCEPT
#allow connect to git in browser
iptables -A OUTPUT -p TCP --sport 80 -m comment --comment "gitlab_http" -j ACCEPT
#allow connect to git in browser with ssl
iptables -A OUTPUT -p TCP --sport 443 -m comment --comment "gitlab_https" -j ACCEPT
#allow git protocol
iptables -A OUTPUT -p TCP --sport 9418 -m comment --comment "gitlab protocol" -j ACCEPT
#allow time sync via NTP
iptables -A OUTPUT -p TCP --dport 123 -m comment --comment "NTP protocol" -j ACCEPT
#allow use Kerberos with TCP
iptables -A OUTPUT -p TCP --dport 88 -m comment --comment "Kerberos TCP" -j ACCEPT
#allow use Kerberos with UDP
iptables -A OUTPUT -p UDP --dport 88 -m comment --comment "Kerberos UDP" -j ACCEPT
#allow change password with Kerberos TCP request
iptables -A OUTPUT -p TCP --dport 464 -m comment --comment "Kerberos password changing TCP" -j ACCEPT
#allow change password with Kerberos UDP request
iptables -A OUTPUT -p UDP --dport 464 -m comment --comment "Kerberos password changing UDP" -j ACCEPT
#allow to use LDAP by TCP
iptables -A OUTPUT -p TCP --dport 389 -m comment --comment "LDAP TCP" -j ACCEPT
#allow to use LDAP by TCP
iptables -A OUTPUT -p UDP --dport 389 -m comment --comment "LDAP UDP" -j ACCEPT
#allow to use encrrypted LDAP by TCP
iptables -A OUTPUT -p TCP --dport 636 -m comment --comment "LDAP SSL" -j ACCEPT
#allow to use Global Catalog in Active Directory
iptables -A OUTPUT -p TCP --dport 3268 -m comment --comment "LDAP GC" -j ACCEPT
#allow to use Global Catalog in Active Directory
iptables -A OUTPUT -p TCP --dport 3269 -m comment --comment "LDAP GC" -j ACCEPT
#allow ping with ipv4
iptables -A OUTPUT -p ICMP --icmp-type 0 -m comment --comment "ping reply" -j ACCEPT

#default rule which drops other outgoing connection
iptables -P OUTPUT DROP
#save to file /etc/sysconfig/iptables
/usr/libexec/iptables/iptables.init save || iptables-save > /etc/sysconfig/iptables