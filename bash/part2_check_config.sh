#!/bin/bash

#install check utility
yum install -y nmap
#set up server to check ports
git_server_address=gitlab.domain.com
#run nmap to scan SSH port and found out its state
if [`nmap $git_server_address -Pn -p 22 | grep '/' | awk '{print $2}'` == open]
then echo "SSH port is opened" 
else echo "SSH port is closed"
fi
#run nmap to scan HTTP port and found out its state
if [`nmap $git_server_address -Pn -p 80 | grep '/' | awk '{print $2}'` == open]
then echo "http port is opened" 
else echo "http port is closed"
fi
#run nmap to scan HTTPS port and found out its state
if [`nmap $git_server_address -Pn -p 443 | grep '/' | awk '{print $2}'` == open]
then echo "https port is opened" 
else echo "https port is closed"
fi
#run nmap to scan GIT port and found out its state
if [`nmap $git_server_address -Pn -p 9418 | grep '/' | awk '{print $2}'` == open]
then echo "GIT port is opened" 
else echo "GIT port is closed"
fi
#run ping to check network connectivity
if [`ping -c 1 $git_server_address | grep packets | awk -F ', ' '{print $2}' | awk '{print $1}'` == 1]
then echo "ping was successful" 
else echo "ping was failed"
fi
