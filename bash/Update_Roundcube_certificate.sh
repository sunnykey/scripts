#! /bin/bash

expiry_date=`certbot-auto certificates | grep 'Expiry Date' | awk '{print $3}'`
cur_date=`date +%Y-%m-%d`
if [ $expiry_date == $cur_date ]
 echo Stop Apache web server for in case of releasing 80 port
 service httpd stop
 echo Renew certificate
 certbot-auto renew
 echo Start Apache Web server
 service httpd start
 echo Restart postfix and dovecot
 service postfix restart
 service dovecot restart
 echo Certificate was updated
else
 echo Certificate is still valid
fi