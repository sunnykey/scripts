@echo off
setlocal enableextensions enabledelayedexpansion

if [%1] == [] (
	call :copy_block In
	call :copy_block Out
	goto :eof
)

:copy_block
set command_file=%tmp%\command.txt

rem Defind below variables
set ftp_user=test
set ftp_password=test
set ftp_server=localhost
set ftp_folder_path=1
set Windows_root_folder_path=c:\tmp
rem End defining variables

cd /d %Windows_root_folder_path%\%1
echo %ftp_user%> %command_file%
echo %ftp_password%>> %command_file%
echo cd %ftp_folder_path%/%1>> %command_file%
echo prompt>> %command_file%
if [%1] == [In] (echo mget *>> %command_file%) else (echo mput *>> %command_file%)
echo bye>> %command_file%
ftp -s:%command_file% %ftp_server%
