@echo off
rem Change path https://github.com/PowerShell/Win32-OpenSSH/releases/latest
set ssh_app_path="C:\Program Files\OpenSSH-Win64"
set ssh_user=root
set ssh_server=172.31.255.233
rem
cd /d %ssh_app_path%
set key=%userprofile%\.ssh\id_rsa
del /f /q %key%
del /s /q %key%.pub
ssh-keygen -t rsa -N "" -f %key%
ssh %ssh_user%@%ssh_server% "mkdir ~/.ssh 2> /dev/null; rm -f ~/.ssh/authorized_keys 2> /dev/null; > ~/.ssh/authorized_keys"
type %key%.pub | ssh %ssh_user%@%ssh_server% "cat >> .ssh/authorized_keys"