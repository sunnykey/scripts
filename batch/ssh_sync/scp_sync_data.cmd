@echo off
setlocal enableextensions enabledelayedexpansion

if [%1] == [] (
	call :copy_block In
	call :copy_block Out
	goto :eof
)

:copy_block

rem Defind below variables
set ssh_user=root
set ssh_server=172.31.255.233
rem You can get ssh folder path by typing command pwd on Linux host
set ssh_folder_path=/root
set Windows_root_folder_path=c:\tmp
rem End defining variables

cd /d %Windows_root_folder_path%\%1
if [%1] == [In] (
 echo Copy data from remote server
 for /f "delims=;" %%i in ('ssh root@172.31.255.233 ^"ls %ssh_folder_path%/In^"') do scp %ssh_user%@%ssh_server%:%ssh_folder_path%/In/%%i ./
) else (
 echo Copy data to remote server	
 for /f "delims=;" %%i in ('dir /b') do scp "%%i" %ssh_user%@%ssh_server%:%ssh_folder_path%/
)
