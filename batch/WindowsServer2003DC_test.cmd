@echo off
set log=c:\tmp\dcdiag.log
mkdir c:\tmp 2> nul
chcp 65001
echo Gathering DC diagnostic
dcdiag /v /e /c /f:%log%
echo !!!!!!!!IPCONFIG!!!!!!!! >> %log%
echo Gathering IP address information
ipconfig /all >> %log%
echo !!!!!!!!repadmin /showrepl!!!!!!!! >> %log%
echo Gathering FSMO roles replication
repadmin /showrepl >> %log%
echo !!!!!!!!/replsummary!!!!!!!! >> %log%
echo Gathering replication summary information
repadmin /replsummary >> %log%
echo !!!!!!!!systeminfo!!!!!!!! >> %log%
echo Gathering system information
systeminfo >> %log%
echo !!!!!!!!eventlog!!!!!!!! >> %log%
echo Gathering EventLog
cd /d %windir%\system32
cscript.exe eventquery.vbs /l application /l system >> %log% 2>nul
echo Please send %log% file to Semen Anikin for diagnostic
ping -n 10 127.0.0.1 > nul