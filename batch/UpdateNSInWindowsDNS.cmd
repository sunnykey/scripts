@echo off
::%1 is mean first script parameter or switch
if "%~1"=="" (
	echo You need pass port as parameter. Example %~n0 domain
	pause
	goto :EOF
)
set ZoneName=%1
::MasterDNSServerName full name of server where all records will be added
set MasterDNSServerName=%COMPUTERNAME%
set ResponsiblePerson=administrator.example.com
::SOA settings
set RefreshInterval=3600
set RertyInterval=3600
set ExpireAfter=86400
set DefaultTTL=3600
set SerialNumber=1
::Parameters for record NS
set PrimaryNSServer=dns.yandex.ru
set SecondaryNSServer=dns.google.com
::Parameters for record MX 
set MXRecordMailServer=mail.%ZoneName%
set MXRecordPriority=10
::Parameters for record A 
set ARecordName=mail
set ARecordIP=172.16.0.1
::Parameters for record CNAME
set CNAMERecordName=www
set CNAMERecordServerName=%ARecordName%.%ZoneName%
:: 1. create the zone
dnscmd %MasterDNSServerName% /zoneadd %ZoneName% /primary
::2. update soa record
::dnscmd /recordadd mydnszone.com @ SOA dns2.sitesolutions.it administrator.sitesolutions.it 1 3600 3600 86400 3600
dnscmd %MasterDNSServerName% /recordadd %ZoneName% @ SOA %PrimaryNSServer% %ResponsiblePerson% %SerialNumber% %RefreshInterval% %RetryInterval% %ExpireAfter% %DefaultTTL% /f
::3. update ns record (primary)
dnscmd %MasterDNSServerName% /recorddelete %ZoneName% @ NS /f 
dnscmd %MasterDNSServerName% /recordadd %ZoneName% @ NS %PrimaryNSServer%
::4. add ns record (secondary)
dnscmd %MasterDNSServerName% /recordadd %ZoneName% @ NS %SecondaryNSServer%
::5. add mx record
dnscmd %MasterDNSServerName% /recordadd %ZoneName% @ MX %MXRecordPriority% %MXRecordMailServer%
::6. add a record
dnscmd %MasterDNSServerName% /recordadd %ZoneName% %ARecordName% A %ARecordIP%
::7. add www CNAME record
dnscmd %MasterDNSServerName% /recordadd %ZoneName% %CNAMERecordName% CNAME %CNAMERecordServerName%
